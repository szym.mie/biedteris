"use strict";
const COLOR_ENUM = [
    "rgba(37, 28, 32, .15)", 
    "#db552a", // red double pill - part is right
    "#1ea1db", // blue double pill
    "#dbaa14", // yellow double pill
    "rgba(37, 28, 32, .15)", 
    "#db552a", // red double pill - part is up
    "#1ea1db", // blue double pill
    "#dbaa14", // yellow double pill
    "rgba(37, 28, 32, .15)", 
    "#db552a", // red double pill - part is left
    "#1ea1db", // blue double pill
    "#dbaa14", // yellow double pill
    "rgba(37, 28, 32, .15)", 
    "#db552a", // red double pill - part is down
    "#1ea1db", // blue double pill
    "#dbaa14", // yellow double pill
    "rgba(37, 28, 32, .15)", 
    "#db552a", // red single pill
    "#1ea1db", // blue single pill
    "#dbaa14", // yellow single pill
    "rgba(37, 28, 32, .15)", // not used
    "#8f3d22", // red virus
    "#005982", // blue virus
    "#8f6d06" // yellow virus
];

const BITMAP_ENUM = [];

const BITMAP_SOURCE_ENUM = [
    "tex/_blank.png", 
    "tex/rdbr.png", // red double pill - part is right
    "tex/bdbr.png", // blue double pill
    "tex/ydbr.png", // yellow double pill
    "tex/_blank.png", 
    "tex/rdbu.png", // red double pill - part is up
    "tex/bdbu.png", // blue double pill
    "tex/ydbu.png", // yellow double pill
    "tex/_blank.png", 
    "tex/rdbl.png", // red double pill - part is left
    "tex/bdbl.png", // blue double pill
    "tex/ydbl.png", // yellow double pill
    "tex/_blank.png", 
    "tex/rdbd.png", // red double pill - part is down
    "tex/bdbd.png", // blue double pill
    "tex/ydbd.png", // yellow double pill
    "tex/_blank.png", 
    "tex/rsin.png", // red single pill
    "tex/bsin.png", // blue single pill
    "tex/ysin.png", // yellow single pill
    "tex/_blank.png", // not used
    ["tex/rvr0.png", "tex/rvr1.png"], // red virus
    ["tex/bvr0.png", "tex/bvr1.png"], // blue virus
    ["tex/yvr0.png", "tex/yvr1.png"], // yellow virus
    "tex/_blank.png",
    "tex/rrng.png", // red ring
    "tex/brng.png", // blue ring
    "tex/yrng.png", // yellow ring
    "tex/_blank.png",
    "tex/rcrs.png", // red cross
    "tex/bcrs.png", // blue cross
    "tex/ycrs.png", // yellow cross
    "tex/0.png", // 0
    "tex/1.png", // 1
    "tex/2.png", // 2
    "tex/3.png", // 3
    "tex/4.png", // 4
    "tex/5.png", // 5
    "tex/6.png", // 6
    "tex/7.png", // 7
    "tex/8.png", // 8
    "tex/9.png", // 9
    // other textures here
    "tex/back.png", // backgorund (42)
    "tex/hndd.png", // hand down (43)
    "tex/hndm.png", // hand middle (44)
    "tex/hndu.png", // hand up (45)
    "tex/scsc.png", // game won (46)
    "tex/gosc.png", // game over (47)
    "tex/_blank.png", // in progress (48)
    "tex/godr.png", // game over dr. (49)
    "tex/_blank.png", // in progress (50)
    [
        "tex/rbv0.png", // red virus big (51)
        "tex/rbv1.png",
        "tex/rbv2.png",
        "tex/rbv3.png"
    ],
    [
        "tex/bbv0.png", // red virus big (52)
        "tex/bbv1.png",
        "tex/bbv2.png",
        "tex/bbv3.png"
    ], 
    [
        "tex/ybv0.png", // red virus big (53)
        "tex/ybv1.png",
        "tex/ybv2.png",
        "tex/ybv3.png"
    ]
];

const SPAWN_ANIM = [
    {rot: 0, pos0: [480, 48], pos1: [496, 48]}, // frame 0
    {rot: 1, pos0: [480, 48], pos1: [480, 32]},
    {rot: 2, pos0: [480, 32], pos1: [464, 32]},
    {rot: 3, pos0: [464, 16], pos1: [464, 32]},
    {rot: 0, pos0: [448, 16], pos1: [464, 16]}, // frame 4
    {rot: 1, pos0: [448, 16], pos1: [448, 0]},
    {rot: 2, pos0: [448, 16], pos1: [432, 16]},
    {rot: 3, pos0: [432, 0], pos1: [432, 16]},
    {rot: 0, pos0: [416, 16], pos1: [432, 16]}, // frame 8
    {rot: 1, pos0: [416, 16], pos1: [416, 0]},
    {rot: 2, pos0: [416, 16], pos1: [400, 16]},
    {rot: 3, pos0: [400, 0], pos1: [400, 16]},
    {rot: 0, pos0: [384, 16], pos1: [400, 16]}, // frame 12
    {rot: 1, pos0: [384, 16], pos1: [384, 0]},
    {rot: 2, pos0: [384, 16], pos1: [368, 16]},
    {rot: 3, pos0: [368, 0], pos1: [368, 16]},
    {rot: 0, pos0: [352, 16], pos1: [368, 16]}, // frame 16
    {rot: 1, pos0: [352, 16], pos1: [352, 0]},
    {rot: 2, pos0: [352, 32], pos1: [336, 32]}, // gravity takes over
    {rot: 3, pos0: [336, 16], pos1: [336, 32]},
    {rot: 0, pos0: [320, 32], pos1: [336, 32]}, // frame 20
    {rot: 0, pos0: [320, 48], pos1: [336, 48]},
    {rot: 0, pos0: [320, 64], pos1: [336, 64]},
    {rot: 0, pos0: [320, 80], pos1: [336, 80]},
];

const COLOR_MASK = 0b11;
const DIRECTION_MASK = 0b11100;
const DIRECTION_RIGHT = 0;
const DIRECTION_UP = 4;
const DIRECTION_LEFT = 8;
const DIRECTION_DOWN = 12;
const DIRECTION_SINGLE = 16;
const DIRECTION_VIRUS = 20;
const DIRECTION_RING = 24;
const DIRECTION_CROSS = 28;

const GAME_WON = 0;
const GAME_OVER = 1;
const GAME_START = 2;

const WIDTH = 8;
const HEIGHT = 17;

const T_NORMAL = 50;
const T_FF = 10;

const RENDERER = {
    can: document.getElementById("canvas"),
    ctx: document.getElementById("canvas").getContext("2d"),
    width: 16,
    offsets: {
        board: [272, 80],
        score: [80, 128],
        hiscr: [80, 80],
        virus: [560, 336],
        hndd: [494, 94],
        hndm: [480, 82],
        hndu: [496, 66],
        mscr: [150, 120],
        godr: [480, 49],
        bvirs: [
            [96, 216],
            [48, 240],
            [48, 304],
            [96, 320],
            [128, 288],
            [128, 256]
        ]
    },
    render: function ()
    {
        const w = this.width;
        const [ox, oy] = this.offsets.board;
        const [sx, sy] = this.offsets.score;
        const [hx, hy] = this.offsets.hiscr;
        const [vx, vy] = this.offsets.virus;
        const [hdx, hdy] = this.offsets.hndd;
        const [hmx, hmy] = this.offsets.hndm;
        const [hux, huy] = this.offsets.hndu;
        const [mx, my] = this.offsets.mscr;
        const [dx, dy] = this.offsets.godr;
        const bvirs = this.offsets.bvirs;
        const bvirl = bvirs.length;
        const s = STATE.width;

        // draw background image here
        this.ctx.globalAlpha = .1;
        this.ctx.drawImage(BITMAP_ENUM[42].data, 0, 0);

        // old vector fill with black color.
        //ctx.fillRect(0, w, s*w, Math.ceil(STATE.board.length/s)*w);

        // draw the board.
        this.ctx.globalAlpha = .3;
        STATE.board.map((c, i) => {
            const m = BITMAP_ENUM[c];
            // old-school no refresh during fast-forward effect.
            this.ctx.drawImage(m.segs ? m.data[Math.floor(STATE.T / 26)] : m.data, i%s*w+ox, Math.floor(i/s)*w+oy);
            // more modern independent timer. (Add 'n' constant of performance.now() before)
            //ctx.drawImage(m.segs ? m.data[n % m.segs] : m.data, i%s*w, Math.floor(i/s)*w);
        });

        // all things atop of background.
        this.ctx.globalAlpha = 1;
        
        const m = Math.floor(STATE.M/4);
        [51, 52, 53].map((c, i) => {
            const b = BITMAP_ENUM[c];
            const p = bvirs[(Math.floor(m/b.segs)+i*2)%bvirl];
            this.ctx.drawImage(b.data[m%b.segs], p[0], p[1]);
        });

        if (STATE.is_spawn)
            switch (STATE.T) 
            {
                case 0:
                case -1:
                    this.ctx.drawImage(BITMAP_ENUM[45].data, hux, huy);
                    break;
                case 49:
                case 48:
                case 47:
                case 5:
                case 4:
                case 3:
                case 2:
                case 1:
                    this.ctx.drawImage(BITMAP_ENUM[44].data, hmx, hmy);
                    break;
                default:
                    const o = Math.floor(STATE.T/10)%2;
                    this.ctx.drawImage(BITMAP_ENUM[43].data, hdx+o, hdy+o*4);
                    break;
            }
        else
        {
            this.ctx.drawImage(BITMAP_ENUM[45].data, hux, huy);
            this.ctx.drawImage(BITMAP_ENUM[STATE.next[0]].data, hux-16, huy-16);
            this.ctx.drawImage(BITMAP_ENUM[STATE.next[1]].data, hux, huy-16);
        }

        this.number(STATE.nwscr, 7, sx, sy, 16);
        this.number(STATE.hiscr, 7, hx, hy, 16);
        this.number(STATE.virus, 2, vx, vy, 16);
        this.ctx.drawImage(BITMAP_ENUM[48+STATE.stat].data, dx, dy);
        this.ctx.drawImage(BITMAP_ENUM[46+STATE.stat].data, mx, my);

        requestAnimationFrame(this.render.bind(this));
    },
    viewport: function ()
    {
        this.can.width = this.width * STATE.width;
        this.can.height = this.width * Math.ceil(STATE.board.length / STATE.width); 
    },
    number: function (n, l, x, y, w)
    {
        [...n.toString().padStart(l, "0")].map((c, i) => {
            this.ctx.drawImage(BITMAP_ENUM[parseInt(c)+32].data, x+i*w, y);
        });
    }
};

const STATE = {
    width: WIDTH,
    active: [],
    next: [Math.floor(Math.random()*3)+1, Math.floor(Math.random()*3)+9],
    board: Array(WIDTH * HEIGHT).fill(0),
    interval: null,
    keys: {"up": false, "down": false, "left": false, "right": false},
    T: T_NORMAL,
    M: 0,
    nwscr: 0,
    hiscr: localStorage.getItem("hi-score") === null ? 0 : localStorage.getItem("hi-score"),
    virus: 10,
    stat: GAME_START,
    is_spawn: false,
    attach: function ()
    {
        addEventListener("keydown", this.keydown.bind(this));
        addEventListener("keyup", this.keyup.bind(this));
    },
    detach: function ()
    {
        removeEventListener("keydown", this.keydown.bind(this));
        removeEventListener("keyup", this.keyup.bind(this));
    },
    tick: function ()
    {
        if (!(this.T%10)) this.M++;
        if (!this.T--) 
        {
            if (this.active.length) 
            {
                this.attach()
                this.is_spawn = false;
                if (this.blocked())
                {
                    this.active = [];
                    this.cure();
                    this.singularize();
                    this.T = T_FF;
                }
                else 
                {
                    this.stepdown();
                    this.T = T_NORMAL;
                }
            }
            else
            {
                this.expire();
                this.cure();
                this.singularize();
                this.is_spawn = !this.gravity() && !this.expired();
                if (this.is_spawn)
                {
                    clearInterval(this.keys.down);
                    this.detach();
                    this.keys.down = false;
                    this.T = T_NORMAL;
    
                } else this.T = T_FF;
            }
        }
        if (this.is_spawn) this.spawn();
    },
    keydown: function (ev)
    {
        const event = fn => { fn(); return setInterval(fn, 200); };
        //if (this.lock) return;
        //this.lock = true
        console.log("keydown");
        ev.preventDefault();
        switch (ev.keyCode)
        {
            case 65: // left
            case 37:
                if (!this.keys.left) this.keys.left = event(this.left.bind(this));
                break;
            case 68: // right
            case 39:
                if (!this.keys.right) this.keys.right = event(this.right.bind(this));
                break;
            case 87: // rotate
            case 38:
                if (!this.keys.up) this.keys.up = event(this.rotate.bind(this));
                break;
            case 83: // down
            case 40:
                if (!this.keys.down) this.keys.down = event(this.down.bind(this));
                break;
            default:
                break;
        };
        //setTimeout(() => {this.lock = false}, 5);
    },
    keyup: function (ev)
    {
        const clear = int => { clearInterval(int); return false; };
        //if (this.lock) return;
        //this.lock = true;
        console.log("keyup");
        ev.preventDefault();
        switch (ev.keyCode)
        {
            case 65: // left
            case 37:
                this.keys.left = clear(this.keys.left);
                break;
            case 68: // right
            case 39:
                this.keys.right = clear(this.keys.right);
                break;
            case 87: // rotate
            case 38:
                this.keys.up = clear(this.keys.up);
                break;
            case 83: // down
            case 40:
                this.keys.down = clear(this.keys.down);
                break;
            default:
                break;
        };
        //setTimeout(() => {this.lock = false}, 5);
    },
    left: function ()
    {
        const b = this.board.slice();
        const o = this.active.map(c => this.board[c]);
        if (this.active.length && !this.left_bordered()) 
        {
            const l = this.active.filter(c => !this.active.includes(c+1))
            this.active = this.active.map(c => c-1);
            l.map(c => {b[c] = 0});
            o.map((c, i) => b[this.active[i]] = c);
            this.board = b;
        }
    },
    right: function ()
    {
        const b = this.board.slice();
        const o = this.active.map(c => this.board[c]);
        if (this.active.length && !this.right_bordered()) 
        {
            const l = this.active.filter(c => !this.active.includes(c-1))
            this.active = this.active.map(c => c+1);
            l.map(c => {b[c] = 0});
            o.map((c, i) => b[this.active[i]] = c);
            this.board = b;
        }
    },
    down: function ()
    {
        this.T = 0;
        this.tick();
        this.T = T_NORMAL;
    },
    score: function (d) 
    {
        const m = 3**d;
        this.nwscr+=(m===1?0:m)*100;
        if (this.nwscr > localStorage.getItem("hi-score"))
        {
            localStorage.setItem("hi-score", this.nwscr);
        }
    },
    cure: function ()
    {
        const vir = this.board.filter(c => (c&DIRECTION_MASK) === DIRECTION_VIRUS).length;
        const map_color = cid => this.board.map(c => (c&COLOR_MASK) == cid);
        let b = this.board.slice();
        const h = Math.ceil(b.length / this.width);
        const del_accum = (arr, board, width) => {
            let m = board.slice();
            arr.reduce((p, e, i) => {
                if (!(i % width))
                {
                    // if (p >= 4) m = m.fill(0, i-p, i);
                    if (p >= 4) m = m.map((c, j) => {
                        let n = c;
                        if (j >= i-p && j < i) switch (c&DIRECTION_MASK)
                        {
                            case DIRECTION_RIGHT:
                            case DIRECTION_UP:
                            case DIRECTION_LEFT:
                            case DIRECTION_DOWN:
                            case DIRECTION_SINGLE:
                                n = DIRECTION_RING + (c&COLOR_MASK);
                                break;
                            case DIRECTION_VIRUS:
                                n = DIRECTION_CROSS + (c&COLOR_MASK);
                                break;
                            case DIRECTION_RING:
                            case DIRECTION_CROSS:
                            default:
                                break;
                        }
                        return n;
                    });
                    p = 0;
                }
    
                p+=e;
                
                // if (p >= 4) m = m.fill(0, i-p+1, m.length - i - 1 ? i : i+1);
                const l = m.length - i - 1 ? i : i + 1;
                if (p >= 4) m = m.map((c, j) => {
                    let n = c;
                    if (j >= i-p+1 && j < l) switch (c&DIRECTION_MASK)
                    {
                        case DIRECTION_RIGHT:
                        case DIRECTION_UP:
                        case DIRECTION_LEFT:
                        case DIRECTION_DOWN:
                        case DIRECTION_SINGLE:
                            n = DIRECTION_RING + (c&COLOR_MASK);
                            break;
                        case DIRECTION_VIRUS:
                            n = DIRECTION_CROSS + (c&COLOR_MASK);
                            break;
                        case DIRECTION_RING:
                        case DIRECTION_CROSS:
                        default:
                            break;
                    }
                    return n;
                });
                if (!e) p = 0;
                return p;
            }, 0);
            return m;
        }
        const transpose = (arr, width, height=width) => {
            const n = [];
            arr.map((c, i) => {
                n[i%width*height+Math.floor(i/width)] = c;
            });
            return n;
        }
        [1,2,3]
            .map(c => {
                const s = map_color(c);
                // horizontal pass
                b = del_accum(s, b, this.width);
    
                // vertical pass
                b = transpose(del_accum(
                    transpose(s, this.width, h), 
                    transpose(b, this.width, h), h), h, this.width);
            });
        this.board = b;
        const nvi = this.board.filter(c => (c&DIRECTION_MASK) === DIRECTION_VIRUS).length;
        this.virus = nvi;
        this.score(vir - nvi);
        if (!nvi) this.gamewon();
    },
    populate: function (n=10)
    {
        const s = [1, 2, 3].map(c => DIRECTION_VIRUS+c);
        const h = Math.floor(this.board.length/this.width*.7);
        const o = (Math.floor(this.board.length/this.width)-h)*this.width;
        const r = this.board.length - o;
        const b = this.board.slice();
        let p = o + Math.floor(Math.random()*r);
        while (n--) 
        {
            while (b[p]) p = o + Math.floor(Math.random()*r);
            b[p] = s[n%3];
        }
        this.board = b;
    },
    gameover: function ()
    {
        clearInterval(this.interval);
        this.detach();
        Object.values(this.keys).map(c => clearInterval(c));
        this.interval = null; 
        this.stat = GAME_OVER;
    },
    gamewon: function ()
    {
        clearInterval(this.interval);
        this.detach();
        Object.values(this.keys).map(c => clearInterval(c));
        this.interval = null; 
        this.stat = GAME_WON;
    },
    singularize: function ()
    {
        const b = this.board.slice();
        const w = this.width;
        const check = o => !(b[o] & COLOR_MASK && b[o] < 16);
    
        b.map((c, i) => {
            if (c & COLOR_MASK)
            {
                const s = (c & COLOR_MASK)+16;
                switch (c & DIRECTION_MASK)
                {
                    case DIRECTION_RIGHT: // right
                        if (check(i+1)) b[i] = s; 
                        break;
                    case DIRECTION_UP: // up
                        if (check(i-w)) b[i] = s; 
                        break;
                    case DIRECTION_LEFT: // left
                        if (check(i-1)) b[i] = s; 
                        break;
                    case DIRECTION_DOWN: // down
                        if (check(i+w)) b[i] = s; 
                        break;
                    case DIRECTION_SINGLE:
                    case DIRECTION_VIRUS:
                    default:
                        break;
                }
            }
        });
    
        this.board = b;
    },
    expire: function ()
    {
        const b = this.board.slice();
        const w = this.width;
        const check = o => b[o] >= DIRECTION_RING;
        this.board = b.map((c, i) => check(i) ? 0 : c);
    },
    expired: function ()
    {
        return this.board.some(c => c >= DIRECTION_RING);
    },
    rotate: function ()
    {
        const b = this.board.slice();
        const horizontal = () => this.active.reduce((p, c) => Math.abs(p-c)) < this.width;
        const ascending = () => this.active.reduce((p, c) => p-c) < 0;
        if (this.active.length && !this.over_bordered())
        {
            const h = horizontal();
            const a = ascending();
            const w = this.width;
            const m = this.active.length-1;
            const t = this.active.map(c => this.board[c]);
            const r = this.active.slice();
            if (!h && !a && this.left_bordered())
                this.right();
            if (!h && a && this.right_bordered())
                this.left();
            if (this.left_bordered() && this.right_bordered() && !h) return;
            this.active = this.active.map(
                (c, i) => h ? 
                    (a ? 
                        (c - w*i + m-i) : 
                        (c - w*(m-i) - m+i)) // horizontal (asc:dsc)
                    : 
                    (a ? 
                        (c + w*(m-i) + i) :
                        (c + w*i - i)) // vertical (asc:dsc)
                );
    
            this.active.map((c, i) => {
                b[c] = (t[i]+4)%16;
            });
            r.filter(c => !this.active.includes(c)).map(c => {b[c] = 0});
            this.board = b;
        }
    },
    gravity: function ()
    {
        const a = this.active.slice(); // backup
        const b = this.board.slice();
        const w = this.width;
        const o = this.board.length-w-1;
        const d = [];
        let f = false;
        this.board.slice(0, o+1).reverse().map((c, i) => {
            this.active = [];
            const r = o-i;
            if (c & COLOR_MASK && c < 20 && !d.includes(r))
            {
                this.active.push(r);
                d.push(r);
                let n;
                switch (c & DIRECTION_MASK) 
                {
                    case DIRECTION_RIGHT:
                        n = r+1;
                        this.active.push(n);
                        d.push(n);
                        break;
                    case DIRECTION_UP:
                        n = r-w;
                        this.active.push(n);
                        d.push(n);
                        break;
                    case DIRECTION_LEFT:
                        n = r-1;
                        this.active.push(n);
                        d.push(n);
                        break;
                    case DIRECTION_DOWN:
                        n = r+w;
                        this.active.push(n);
                        d.push(n);
                        break;
                    case DIRECTION_SINGLE:
                    default:
                        break;
                }
                if (!this.blocked()) 
                {
                    this.stepdown();
                    f = true;
                }
            }
        });
        //this.board = b.reverse();
        this.active = a;
        return f;
    },
    stepdown: function ()
    {
        const b = this.board.slice();
        const o = this.active.map(c => this.board[c]);
        this.active = this.active.map(c => {
            b[c] = 0;
            return c+this.width;
        });
        o.map((c, i) => {b[this.active[i]] = c});
        this.board = b;
    },
    blocked: function ()
    {
        if (this.active.length)
            return this.active
                .filter(c => !this.active.includes(c+this.width))
                .map(c => c+this.width)
                .some(c => this.board[c] > 0 || this.board[c] == undefined);
        return false;
    },
    left_bordered: function ()
    {
        if (this.active.length)
            return this.active
                .filter(c => !this.active.includes(c-1))
                .some(c => this.board[c-1] || !(c%this.width));
        return false;
    },
    right_bordered: function ()
    {
        if (this.active.length)
            return this.active
                .filter(c => !this.active.includes(c+1))
                .some(c => this.board[c+1] || !((c+1)%this.width));
        return false;
    },
    over_bordered: function ()
    {
        if (this.active.length)
            return this.active
                .filter(c => !this.active.includes(c-this.width))
                .map(c => c-this.width)
                .some(c => this.board[c] > 0);
        return false;
    },
    spawn: function ()
    {
        const T = Math.floor((T_NORMAL - this.T)/2);
        if (T > SPAWN_ANIM.length) 
        {
            const b = this.board.slice();
            const index = Math.floor(this.width / 2)+this.width;
            if (!(b[index-1] || b[index]))
            {
                b[index-1] = this.next[0];
                b[index] = this.next[1];
                this.next = [Math.floor(Math.random()*3)+1, Math.floor(Math.random()*3)+9];
            }
            else this.gameover();
            this.active = [index-1, index];
            this.board = b;
        }
        else
        {
            const s = SPAWN_ANIM[T];
            if (s !== undefined) {
                RENDERER.ctx.drawImage(BITMAP_ENUM[this.next[0]+s.rot*4].data, s.pos0[0], s.pos0[1]);
                RENDERER.ctx.drawImage(BITMAP_ENUM[(this.next[1]+s.rot*4)%16].data, s.pos1[0], s.pos1[1]);   
            }
        }
    },
    start: function ()
    {
        this.interval = setInterval(this.tick.bind(this), 10);
        this.attach();
    },
    pause: function ()
    {
        clearTimeout(this.interval);
        this.detach();
    }    
};

const image = url => 
    new Promise((resolve, reject) => {
        const i = new Image();
        i.onload = function () {
            resolve(createImageBitmap(this));
        };
        i.onerror = function () {
            reject("not found");
        }
        i.src = url;
    });

const decompose = img =>
    new Promise((resolve, reject) => {
        switch (typeof img)
        {
            case "string":
                resolve(image(img));
                break;
            case "object":
                resolve(Promise.all(img.map(c => image(c))));
                break;
            default:
                reject("invalid argument");
                break;
        }
    });


RENDERER.ctx.strokeStyle = "#a28792";

async function init() 
{
    await Promise.all(BITMAP_SOURCE_ENUM.map(c => decompose(c)))
        .then(data => data.map((c, i) => BITMAP_ENUM[i] = {segs: c instanceof ImageBitmap ? 0 : c.length, data: c}))
        .catch(err => {console.error("Some graphics are missing.\nAborting.")});
    STATE.populate(10);
    //viewport();
    RENDERER.render();
    STATE.start();
}

setTimeout(init, 1000);